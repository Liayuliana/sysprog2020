/* Source code nomor 6 */

# include <stdio.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <string.h>

static char* str = "Hello\n";

int main(int argc, char **argv) {

    // Cek apakah user sudah menginput argument dengan benar
    // Argument dari user berformat <namafile>.txt
    // Argumen ini nantinya akan dijadikan nama file
    if(argc < 2) {
        printf("Invalid argument\n");
    } else if (argc > 2) {
        printf("Invalid argument\n");
    } else {
        // Menginisiasi file descriptor
        int fd;

        // Membuat atau membuka file dengan file descriptor
        fd = open (argv[1], O_RDWR | O_CREATE, 0644);

        printf("File descriptor STDOUT_FILENO: %d\n", STDOUT_FILENO);
        printf("File descriptor fd: %d\n", fd);
        printf("If I had more time\n");
        write(STDOUT_FILENO, "I would have written you a letter\n", 43);
        write(fd, str, strlen(str));
        close(fd);
        printf("See output in file %s\n", argv[1]);
    }
}
